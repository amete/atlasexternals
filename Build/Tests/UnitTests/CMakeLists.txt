# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

atlas_subdir( UnitTests )

#
# Tests for unit_test_executor.sh
#
atlas_add_test( utexec_success
   SCRIPT "exit 0"
   LABELS "Successful" )

atlas_add_test( utexec_fail
   SCRIPT "exit 1"
   PROPERTIES WILL_FAIL TRUE
   LABELS "Failure" )

atlas_add_test( utexec_fail_dummypost
   SCRIPT "exit 1"
   POST_EXEC_SCRIPT "# no post-processing"
   PROPERTIES WILL_FAIL TRUE
   LABELS "Successful" "Failure" )

atlas_add_test( utexec_success_dummypost
   SCRIPT "exit 0"
   POST_EXEC_SCRIPT "# no post-processing" )

atlas_add_test( utexec_testStatus
   SCRIPT "exit 42"
   POST_EXEC_SCRIPT "echo \${testStatus}"
   PROPERTIES PASS_REGULAR_EXPRESSION "42" )

atlas_add_test( utexec_success_post_fail
   SCRIPT "exit 0"
   POST_EXEC_SCRIPT "exit 1"
   PROPERTIES WILL_FAIL TRUE )

atlas_add_test( utexec_fail_post_success
   SCRIPT "exit 1"
   POST_EXEC_SCRIPT "exit 0" )

atlas_add_test( utexec_fail_nopost
   SCRIPT "exit 1"
   POST_EXEC_SCRIPT "exit \${testStatus}"
   PROPERTIES WILL_FAIL TRUE )

atlas_add_test( utexec_preexec
   SCRIPT "exit 0"
   PRE_EXEC_SCRIPT "echo Hello"
   PROPERTIES PASS_REGULAR_EXPRESSION "Hello" )

atlas_add_test( utexec_testname
   SCRIPT "test \${ATLAS_CTEST_PACKAGE} = UnitTests -a "
          "\${ATLAS_CTEST_TESTNAME} = utexec_testname " )

atlas_add_test( utexec_logpattern
   SCRIPT "test \${ATLAS_CTEST_LOG_SELECT_PATTERN} = '^.*pat1$' -a "
          "\${ATLAS_CTEST_LOG_IGNORE_PATTERN} = 'pat2.*|pat3'"
   LOG_SELECT_PATTERN "^.*pat1$"
   LOG_IGNORE_PATTERN "pat2.*|pat3" )

atlas_add_test( utexec_environment
   SCRIPT "echo -n \${FOO} && echo -n \${BAR}"
   ENVIRONMENT FOO=foo BAR=bar
   POST_EXEC_SCRIPT "logContent=`cat utexec_environment.log`
if [[ \"$logContent\" != \"foobar\" ]]
then
   exit 1
fi" )

# Test dependency tests:
atlas_add_test( test_depends1
   # sleep to check if depends2 really runs afterwards
   SCRIPT "sleep 1 && echo 1" > test_depends.log )

atlas_add_test( test_depends2
   SCRIPT "echo 2" >> test_depends.log
   DEPENDS test_depends1 )

atlas_add_test( test_depends3
   SCRIPT "echo 3" >> test_depends.log
   DEPENDS test_depends1 test_depends2
   POST_EXEC_SCRIPT "echo '1\n2\n3' | diff test_depends.log -" )

#
# find-module tests
#

# Always ignore these packages:
set( _ignore )
list( APPEND _ignore "COOL;CORAL" )         # in tdaq-common release
list( APPEND _ignore "CUDAToolkit;cuDNN" )  # requires CUDA
list( APPEND _ignore "Qt4" )                # no longer supported
list( APPEND _ignore "SoQt" )               # built in atlasexternals
list( APPEND _ignore "HJets" )              # only available for gcc8
list( APPEND _ignore "dcap;gfal;srm_ifce" ) # grid tools not available on el9
list( APPEND _ignore "libzip" )             # only available if Sherpa>=3
list( APPEND _ignore "PNG" )                # depends on libzip
list( APPEND _ignore "MCFM" )               # Is not in LCG_104 but only from layer LCG_104c_ATLAS_3 onwards
list( APPEND _ignore "HighFive" )           # Is not in LCG_104 but only from layer LCG_104c_ATLAS_9 onwards

# LCG base release + ALRB
atlas_add_test( find_modules_lcg
   SCRIPT source \${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh &&
          lsetup cppcheck &&
          ${CMAKE_COMMAND} --fresh -DLCG_VERSION_NUMBER="104"
            -DATLAS_LCGTEST_FIND_MODE=REQUIRED
            -DATLAS_LCGTEST_IGNORE="${_ignore}"
            ${CMAKE_CURRENT_SOURCE_DIR}/../../AtlasLCG/test
   PRIVATE_WORKING_DIRECTORY
   PROPERTIES TIMEOUT 300 )
