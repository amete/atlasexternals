# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# Package building nlohmann_json as part of the offline / analysis software
# build.
#

# The name of the package:
atlas_subdir( nlohmann_json )

# Check whether the package is meant to be built or not.
if( NOT ATLAS_BUILD_NLOHMANN_JSON )
   return()
endif()
message( STATUS "Building nlohmann_json as part of the project" )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# Declare where to get nlohmann_json from.
set( ATLAS_NLOHMANN_JSON_SOURCE
   "URL;https://cern.ch/lcgpackages/tarFiles/sources/json-3.10.5.tar.gz;URL_MD5;5b946f7d892fa55eabec45e76a20286b"
   CACHE STRING "The source for nlohmann_json" )
mark_as_advanced( ATLAS_NLOHMANN_JSON_SOURCE )

# Decide whether / how to patch the nlohmann_json sources.
set( ATLAS_NLOHMANN_JSON_PATCH "" CACHE STRING
   "Patch command for nlohmann_json" )
set( ATLAS_NLOHMANN_JSON_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of nlohmann_json (2022.10.11.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_NLOHMANN_JSON_PATCH
   ATLAS_NLOHMANN_JSON_FORCEDOWNLOAD_MESSAGE )

# Temporary directory for the build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/jsonBuild" )

# Set up extra CMake cache values.
set( _extraArgs )
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()
if( NOT "${CMAKE_CXX_STANDARD}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()

# Build nlohmann_json for the build area:
ExternalProject_Add( nlohmann_json
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_NLOHMANN_JSON_SOURCE}
   ${ATLAS_NLOHMANN_JSON_PATCH}
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DJSON_BuildTests:BOOL=false # switch off the build of the tests
   ${_extraArgs}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( nlohmann_json forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo
   "${ATLAS_NLOHMANN_JSON_FORCEDOWNLOAD_MESSAGE}"
   DEPENDERS download )
ExternalProject_Add_Step( nlohmann_json purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for nlohmann_json"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( nlohmann_json forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f "<BINARY_DIR>/CMakeCache.txt"
   COMMENT "Forcing the configuration of nlohmann_json"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( nlohmann_json buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing nlohmann_json into the build area"
   DEPENDEES install )
add_dependencies( Package_nlohmann_json nlohmann_json )

# And now install it:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
