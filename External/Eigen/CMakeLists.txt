# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# Package building Eigen as part of the offline / analysis software build.
#

# The name of the package:
atlas_subdir( Eigen )

# Stop if the build is not needed:
if( NOT ATLAS_BUILD_EIGEN )
   return()
endif()

# Tell the user what's happening:
message( STATUS "Building Eigen as part of this project" )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# Declare where to get Eigen from.
set( ATLAS_EIGEN_SOURCE
   "URL;http://cern.ch/lcgpackages/tarFiles/sources/eigen-3.4.0.tar.gz;URL_MD5;4c527a9171d71a72a9d4186e65bea559"
   CACHE STRING "The source for Eigen" )
mark_as_advanced( ATLAS_EIGEN_SOURCE )

# Decide whether / how to patch the Eigen sources.
set( ATLAS_EIGEN_PATCH "" CACHE STRING "Patch command for Eigen" )
set( ATLAS_EIGEN_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of Eigen (2023.07.26.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_EIGEN_PATCH ATLAS_EIGEN_FORCEDOWNLOAD_MESSAGE )

# Temporary directory for the build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/EigenBuild" )

# Extra options for the configuration:
set( _extraOptions )
if( "${CMAKE_CXX_STANDARD}" VERSION_GREATER_EQUAL "11" )
   list( APPEND _extraOptions
      "-DEIGEN_BUILD_CXXSTD:STRING=-std=c++${CMAKE_CXX_STANDARD}" )
endif()

# Build Eigen for the build area:
ExternalProject_Add( Eigen
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_EIGEN_SOURCE}
   ${ATLAS_EIGEN_PATCH}
   CMAKE_CACHE_ARGS -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   ${_extraOptions}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( Eigen forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_EIGEN_FORCEDOWNLOAD_MESSAGE}"
   DEPENDERS download )
ExternalProject_Add_Step( Eigen purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for Eigen"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( Eigen forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f "<BINARY_DIR>/CMakeCache.txt"
   COMMENT "Forcing the configuration of Eigen"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( Eigen buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing Eigen into the build area"
   DEPENDEES install )
add_dependencies( Package_Eigen Eigen )

# And now install it:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
