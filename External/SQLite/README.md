SQLite
======

This package builds [SQLite](https://www.sqlite.org) for "standalone" projects
that require it.
