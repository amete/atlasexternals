# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# Package building cppcheck for the offline builds.
#

# The name of the package:
atlas_subdir( cppcheck )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# Declare where to get CppCheck from.
set( ATLAS_CPPCHECK_SOURCE
   "URL;https://cern.ch/atlas-software-dist-eos/externals/cppcheck/2.6.tar.gz;URL_MD5;11154ab69f88d3f6b275233cd30cb540"
   CACHE STRING "The source for CppCheck" )
mark_as_advanced( ATLAS_CPPCHECK_SOURCE )

# Decide whether / how to patch the CppCheck sources.
set( ATLAS_CPPCHECK_PATCH "" CACHE STRING "Patch command for CppCheck" )
set( ATLAS_CPPCHECK_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of CppCheck (2022.10.11.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_CPPCHECK_PATCH ATLAS_CPPCHECK_FORCEDOWNLOAD_MESSAGE )

# Directory for the temporary build results.
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/cppcheckBuild" )

# Build cppcheck for the build area:
ExternalProject_Add( cppcheck
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_CPPCHECK_SOURCE}
   ${ATLAS_CPPCHECK_PATCH}
   CMAKE_CACHE_ARGS
   -DCMAKE_BUILD_TYPE:STRING=Release
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DUSE_MATCHCOMPILER:BOOL=ON
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( cppcheck forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_CPPCHECK_FORCEDOWNLOAD_MESSAGE}"
   DEPENDERS download )
ExternalProject_Add_Step( cppcheck purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for CppCheck"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( cppcheck forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f "<BINARY_DIR>/CMakeCache.txt"
   COMMENT "Forcing the (re-)configuration of CppCheck"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( cppcheck buildinstall
   COMMAND ${CMAKE_COMMAND} -E remove_directory
   "${_buildDir}/cmake"
   COMMAND ${CMAKE_COMMAND} -E copy_directory
   "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing CppCheck into the build area"
   DEPENDEES install )
add_dependencies( Package_cppcheck cppcheck )

# Install cppcheck:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
