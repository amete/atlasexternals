VecGeom
=======

This package builds VecGeom for use by ATLAS's Geant4 build.

The sources are picked up from VecGeom's GitLab development site, and
are currently built with default flags. This means it also builds and
installs its own version of the VecCore library.

