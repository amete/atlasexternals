// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

#pragma ATLAS check_thread_safety

void i1 [[ATLAS::check_thread_safety_debug]] () {}

struct [[ATLAS::not_thread_safe]] [[ATLAS::check_thread_safety_debug]] C3
{
  void j1 [[ATLAS::check_thread_safety_debug]] () {}
};

