Test Externals Project
======================

This project is here to provide a convenient way of testing changes to the
`AtlasCMake` and `AtlasLCG` "packages" without having to build a big project
first.

It's meant for developing the CMake code of this repository, no general
description is given about it here.
