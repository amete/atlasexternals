# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Configuration options for building the AthAnalysis externals. Collected
# into a single place.
#

# Look for appropriate externals.
find_package( nlohmann_json QUIET )

# Decide whether to build nlohmann_json.
set( _flag FALSE )
if( NOT nlohmann_json_FOUND )
   set( _flag TRUE )
endif()
option( ATLAS_BUILD_NLOHMANN_JSON
   "Build nlohmann_json as part of the release" ${_flag} )

# Default build options.
option( ATLAS_BUILD_DAVIX "Build Davix as part of the release" OFF )
option( ATLAS_BUILD_XROOTD "Build XRootD as part of the release" OFF )
option( ATLAS_BUILD_FASTJET
   "Build FastJet/FJContrib as part of this project" OFF )
option( ATLAS_BUILD_CLHEP
   "Build CLHEP as part of this project" ON )
find_package( CORAL QUIET )
if( CORAL_FOUND )
   option( ATLAS_BUILD_CORAL
      "Build CORAL as part of this project" FALSE )
else()
   option( ATLAS_BUILD_CORAL
      "Build CORAL as part of this project" TRUE )
endif()
find_package( COOL QUIET )
if( COOL_FOUND AND ( NOT ATLAS_BUILD_CORAL ) )
   option( ATLAS_BUILD_COOL
      "Build COOL as part of this project" FALSE )
else()
   option( ATLAS_BUILD_COOL
      "Build COOL as part of this project" TRUE )
endif()

# Make CMake forget that it found any of these packages. (In case it did.)
# Since they could interfere with the environment setup of the project.
# Whichever package ends up needing those externals, will anyway ask for
# them explicitly.
get_property( _packages GLOBAL PROPERTY PACKAGES_FOUND )
list( REMOVE_ITEM _packages nlohmann_json CORAL COOL )
set_property( GLOBAL PROPERTY PACKAGES_FOUND ${_packages} )
unset( _packages )
